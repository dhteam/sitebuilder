<?php
define('MAX_FILE_LIMIT', 1024 * 1024 * 10);//10 Megabytes max html file size
function dd($value) {
    var_dump($value);exit;
}
require 'vendor/autoload.php';

$files = [];
foreach (glob(__DIR__ . "/*.html") as $file) {
    $files[] = $file;
}


$datos = file_get_contents("datos.json");
$datos = json_decode($datos);

//sanitize, remove double dot .. and remove get parameters if any
$fileName = __DIR__ . '/' . preg_replace('@\?.*$@' , '', preg_replace('@\.{2,}@' , '', preg_replace('@[^\/\\a-zA-Z0-9\-\._]@','', $_POST['fileName'])));

$html = $_POST['html'];
$gtm_id = $_POST['gtm-id'];
// Parse the document. $dom is a DOMDocument.
$dom = new IvoPetkov\HTML5DOMDocument('1.0', 'UTF-8');
$dom->loadHTML($html);

$selected = $dom->querySelector('.selected-element');
if ($selected !== null) {
    $selected->setAttribute('contenteditable', 'false');
}


if (strlen($gtm_id) > 0) {
    $datos->GTM = $gtm_id;
    file_put_contents("datos.json", json_encode($datos));
}

$dom->saveHTMLFile($fileName);

foreach ($files as $file) {
    
    if (strlen($gtm_id) > 0) {
        $html_buffer = file_get_contents($file);
        
        $dom_buffer = new IvoPetkov\HTML5DOMDocument('1.0', 'UTF-8');
        
        $dom_buffer->loadHTML($html_buffer);
        
        $old_gtm = $dom_buffer->querySelector('#GTM');
        $old_gtm2 = $dom_buffer->querySelector('#GTM2');
        $head = $dom_buffer->querySelector('head');
        $body = $dom_buffer->querySelector('body');
        // dd($body->childNodes->item(0));
        
        if ($old_gtm !== null) {
            $head->removeChild($old_gtm);
        }
        
        if ($old_gtm2 !== null) {
            $body->removeChild($old_gtm2);
        }
        
        $gtm = "let iframe = false;
        iframe = window.top !== window.self;
        if (!iframe) {
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','" . $gtm_id . "');
        }";
        
        $gtm2 = "<iframe src='https://www.googletagmanager.com/ns.html?id=" . $gtm_id . "' height='0' width='0' style='display:none;visibility:hidden'></iframe>";

            $gtm_noscript_iframe = $dom_buffer->createElement('iframe');
            $gtm_noscript_iframe->setAttribute('src', "https://www.googletagmanager.com/ns.html?id=" . $gtm_id);
            $gtm_noscript_iframe->setAttribute('height', "0");
            $gtm_noscript_iframe->setAttribute('width', "0");
            $gtm_noscript_iframe->setAttribute('style', "display:none;visibility:hidden;");

            
            $gtm_script = $dom_buffer->createElement('script', $gtm);
            $gtm_script->setAttribute('id', 'GTM');
            $head->appendChild($gtm_script);
            
            $gtm_noscript = $dom_buffer->createElement('noscript', "");
            $gtm_noscript->setAttribute('id', 'GTM2');
            $gtm_noscript->appendChild($gtm_noscript_iframe);
            
            $body->insertBefore($gtm_noscript, $body->firstChild);
            
            $dom_buffer->saveHTMLFile($file);
            
        }
    }
    