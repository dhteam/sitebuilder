<?php
$datos= file_get_contents("datos.json");
$datos=json_decode($datos,true);

if ($_POST) {
    if ($datos['resp'] === $_POST['resp']) {
        echo json_encode(['pass' => $datos['password']]);exit;
    } else {
        echo json_encode(['error' => 'La respuesta no es correcta.']);exit;
    }
    
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Recuperar Contraseña</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <style media="screen">
        form {
            border: 3px solid #f1f1f1;
        }
        
        /* Full-width inputs */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        
        /* Set a style for all buttons */
        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }
        
        /* Add a hover effect for buttons */
        button:hover {
            opacity: 0.8;
        }
        
        /* Extra style for the cancel button (red) */
        .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
        }
        
        /* Center the avatar image inside this container */
        .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
        }
        
        /* Avatar image */
        img.avatar {
            width: 40%;
            border-radius: 50%;
        }
        
        /* Add padding to containers */
        .container {
            padding: 16px;
        }
        
        /* The "Forgot password" text */
        span.psw {
            float: right;
            padding-top: 16px;
        }
        
        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }
            .cancelbtn {
                width: 100%;
            }
        }
    </style>
</head>
<body>  
        <div class="container" >
            <form style="border: none;">
                <h2>Recuperá tu contraseña respondiendo la pregunta que creaste al instalar tu sitio:</h2>
                <h4><?= $datos['preg']?></h4>
                <input type="text" name="resp" placeholder="Ingresá acá tu respuesta secreta.">
                <button type="submit" id="recover">Recuperar Contraseña</button>
            </form>
        </div>

        <script>
            let button = document.querySelector('#recover');
            button.addEventListener('click', (e) => {
                e.preventDefault();
                fetch('recover.php', {
                    method: 'POST',
                    body: new FormData(document.querySelector('form'))
                })
                .then(data => data.json())
                .then(data => {
                    if (data.pass) {
                        Swal.fire(
                        'Respuesta correcta',
                        `Tu password es: ${data.pass}`,
                        'success'
                        )
                    } else if (data.error) {
                        Swal.fire(
                        'Respuesta Incorrecta',
                        '',
                        'error'
                        )
                    }
                })
                
                // 
            })
        </script>
</body>
</html>
