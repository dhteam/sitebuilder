<?php
    define('MAX_FILE_LIMIT', 1024 * 1024 * 10);
    $datos = file_get_contents("datos.json.dist");
    $datos = json_decode($datos,true);

    $version = file_get_contents("version.txt");

    $post_data = [
        "name" => str_replace('.xyz', '', $_SERVER['SERVER_NAME']),
        "type" => $_POST['tipo'],
        "url" => str_replace('index.php','', $_SERVER['HTTP_REFERER']),
        "sitebuilder_version" => $version
    ];


    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,"https://blocks.digitalhouse.com/api/sites");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
// In real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// Receive server response ...
    curl_exec($ch);
    curl_close ($ch);


    if (isset($_POST["tipo"])) {
        if (count($datos["pages"])==0) {
            //rename("index.php", "reinstall.php");
            switch ($_POST["tipo"]) {
                case 'institucional':
                    for ($i=0; $i < 5; $i++) {
                        switch ($i) {
                            case 0:
                                $temp=[
                                    "name" => "Inicio",
                                    "title" => "Inicio",
                                    "url" => "index.html"
                                ];
                                $datos["pages"][] = $temp;
                                $contents = fopen("templates/institucional.html","r");
                                $html = fread($contents,filesize("templates/institucional.html"));
                                fclose($contents);
                                fopen("index.html", "w");
                                chmod("index.html",0777);
                                $f = fopen("index.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 1:
                                $temp = [
                                    "name" => "Nosotros",
                                    "title" => "Nosotros",
                                    "url" => "nosotros.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/about.html","r");
                                $html = fread($contents,filesize("templates/about.html"));
                                fclose($contents);
                                fopen("nosotros.html", "w");
                                chmod("nosotros.html",0777);
                                $f = fopen("nosotros.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 2:
                                $temp = [
                                    "name" => "Contacto",
                                    "title" => "Contacto",
                                    "url" => "contacto.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/formpage.html","r");
                                $html = fread($contents,filesize("templates/formpage.html"));
                                fclose($contents);
                                fopen("contacto.html", "w");
                                chmod("contacto.html",0777);
                                $f = fopen("contacto.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 3:
                                $temp = [
                                    "name" => "Precio",
                                    "title" => "Precio",
                                    "url" => "precio.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/pricing.html","r");
                                $html = fread($contents,filesize("templates/pricing.html"));
                                fclose($contents);
                                fopen("precio.html", "w");
                                chmod("precio.html",0777);
                                $f = fopen("precio.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 4:
                                $temp = [
                                    "name" => "Gracias",
                                    "title" => "Gracias",
                                    "url" => "gracias.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/gracias.html","r");
                                $html = fread($contents,filesize("templates/gracias.html"));
                                fclose($contents);
                                fopen("gracias.html", "w");
                                chmod("gracias.html",0777);
                                $f = fopen("gracias.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            default:
                                // code...
                                break;
                        }

                    }
                    break;
                case 'productos':
                    for ($i=0; $i < 5; $i++) {
                        switch ($i) {
                            case 0:
                                $temp = [
                                    "name" => "Inicio",
                                    "title" => "Inicio",
                                    "url" => "index.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/shop.html","r");
                                $html = fread($contents,filesize("templates/shop.html"));
                                fclose($contents);
                                fopen("index.html", "w");
                                chmod("index.html",0777);
                                $f = fopen("index.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 1:
                                $temp = [
                                    "name" => "Nosotros",
                                    "title" => "Nosotros",
                                    "url" => "nosotros.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/about.html","r");
                                $html = fread($contents,filesize("templates/about.html"));
                                fclose($contents);
                                fopen("nosotros.html", "w");
                                chmod("nosotros.html",0777);
                                $f = fopen("nosotros.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 2:
                                $temp = [
                                    "name" => "Contacto",
                                    "title" => "Contacto",
                                    "url" => "contacto.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/formpage.html","r");
                                $html = fread($contents,filesize("templates/formpage.html"));
                                fclose($contents);
                                fopen("contacto.html", "w");
                                chmod("contacto.html",0777);
                                $f = fopen("contacto.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 3:
                                $temp = [
                                    "name" => "Producto",
                                    "title" => "Producto",
                                    "url" => "producto.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/producto.html","r");
                                $html = fread($contents,filesize("templates/producto.html"));
                                fclose($contents);
                                fopen("producto.html", "w");
                                chmod("producto.html",0777);
                                $f = fopen("producto.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 4:
                                $temp = [
                                    "name" => "Gracias",
                                    "title" => "Gracias",
                                    "url" => "gracias.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/gracias.html","r");
                                $html = fread($contents,filesize("templates/gracias.html"));
                                fclose($contents);
                                fopen("gracias.html", "w");
                                chmod("gracias.html",0777);
                                $f = fopen("gracias.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            default:
                                // code...
                                break;
                        }

                    }
                    break;
                case 'app':
                    for ($i=0; $i < 4; $i++) {
                        switch ($i) {
                            case 0:
                                $temp = [
                                    "name" => "Inicio",
                                    "title" => "Inicio",
                                    "url" => "index.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/app.html","r");
                                $html = fread($contents,filesize("templates/app.html"));
                                fclose($contents);
                                fopen("index.html", "w");
                                chmod("index.html",0777);
                                $f = fopen("index.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 1:
                                $temp = [
                                    "name" => "Nosotros",
                                    "title" => "Nosotros",
                                    "url" => "nosotros.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/about.html","r");
                                $html = fread($contents,filesize("templates/about.html"));
                                fclose($contents);
                                fopen("nosotros.html", "w");
                                chmod("nosotros.html",0777);
                                $f = fopen("nosotros.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 2:
                                $temp = [
                                    "name" => "Contacto",
                                    "title" => "Contacto",
                                    "url" => "contacto.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/formpage.html","r");
                                $html = fread($contents,filesize("templates/formpage.html"));
                                fclose($contents);
                                fopen("contacto.html", "w");
                                chmod("contacto.html",0777);
                                $f = fopen("contacto.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 3:
                                $temp = [
                                    "name" => "Gracias",
                                    "title" => "Gracias",
                                    "url" => "gracias.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/gracias.html","r");
                                $html = fread($contents,filesize("templates/gracias.html"));
                                fclose($contents);
                                fopen("gracias.html", "w");
                                chmod("gracias.html",0777);
                                $f = fopen("gracias.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            default:
                                // code...
                                break;
                        }

                    }
                    break;
                case 'blog':
                    for ($i=0; $i < 5; $i++) {
                        switch ($i) {
                            case 0:
                                $temp = [
                                    "name" => "Inicio",
                                    "title" => "Inicio",
                                    "url" => "index.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/blog.html","r");
                                $html = fread($contents,filesize("templates/blog.html"));
                                fclose($contents);
                                fopen("index.html", "w");
                                chmod("index.html",0777);
                                $f = fopen("index.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 1:
                                $temp = [
                                    "name" => "Post",
                                    "title" => "Post",
                                    "url" => "post.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/post.html","r");
                                $html = fread($contents,filesize("templates/post.html"));
                                fclose($contents);
                                fopen("post.html", "w");
                                chmod("post.html",0777);
                                $f = fopen("post.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 2:
                                $temp = [
                                    "name" => "Nosotros",
                                    "title" => "Nosotros",
                                    "url" => "nosotros.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/about.html","r");
                                $html = fread($contents,filesize("templates/about.html"));
                                fclose($contents);
                                fopen("nosotros.html", "w");
                                chmod("nosotros.html",0777);
                                $f = fopen("nosotros.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 3:
                                $temp = [
                                    "name" => "Contacto",
                                    "title" => "Contacto",
                                    "url" => "contacto.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/formpage.html","r");
                                $html = fread($contents,filesize("templates/formpage.html"));
                                fclose($contents);
                                fopen("contacto.html", "w");
                                chmod("contacto.html",0777);
                                $f = fopen("contacto.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            case 4:
                                $temp = [
                                    "name" => "Gracias",
                                    "title" => "Gracias",
                                    "url" => "gracias.html"
                                ];
                                $datos["pages"][]=$temp;
                                $contents = fopen("templates/gracias.html","r");
                                $html = fread($contents,filesize("templates/gracias.html"));
                                fclose($contents);
                                fopen("gracias.html", "w");
                                chmod("gracias.html",0777);
                                $f = fopen("gracias.html", "w+") or die("fopen failed");
                                fwrite($f, $html);
                                fclose($f);
                                break;
                            default:
                                // code...
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        $datos["email"]=$_POST["email"];
        $datos["password"]=$_POST["password"];
        $datos["preg"]=$_POST["preg"];
        $datos["resp"]=$_POST["resp"];
        json_encode($datos["pages"]);
        $datos=json_encode($datos);
        file_put_contents("datos.json", $datos);
        session_start();
        header("Location:login.php");
    }
    if(!isset($_SESSION["login"])){
        header("Location:login.php");
    }
