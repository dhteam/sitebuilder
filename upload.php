<?php
//This script is used by image upload input to save the imge on the server and return the image url to be set as image src attribute.

define('UPLOAD_FOLDER', __DIR__ . '/uploads'.'/');
define('UPLOAD_PATH', '/uploads'.'/');

$_FILES['file']['name'] = str_replace( '.jpeg', '.jpg', $_FILES['file']['name']);
move_uploaded_file($_FILES['file']['tmp_name'], UPLOAD_FOLDER . $_FILES['file']['name']);

echo 'uploads'.'/' . $_FILES['file']['name'];
