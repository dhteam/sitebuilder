<?php
if(file_exists("datos.json")){
$datos= file_get_contents("datos.json");
$datos=json_decode($datos,true);

if(count($datos["pages"])>0){
   echo '<script>window.location.href = "index.html";</script>';
}
}
 ?>
 <!DOCTYPE html>
 <html lang="en" dir="ltr">
   <head>
     <meta charset="utf-8">
     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:300,400,700" rel="stylesheet">
     <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
     <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
     <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
     <link rel="stylesheet" href="css/main.css">
     <title>Digital House - Site Builder</title>
     <style media="screen">
     .check
     {
       opacity:0.5;
       color:#996;
     }
     .box{
       margin-bottom:5px;
     }
     </style>
   </head>
   <body>
     <center>
       <img src="img/logo.png" alt="">
     <h1>Seleccione el tipo de sitio:</h1>
     <form class="" action="installer.php" method="post">
       <div class="form-group" style="width:100%">
         <div class="col-md-1 box"></div>
         <div class="col-md-2 box"></div>
         <div class="col-md-2 box"><label class="btn btn-primary btn1"><h3>Institucional</h3> <img src="img/building.png" alt="..." class="img-thumbnail img-check"><input type="radio" required name="tipo" id="item4" value="institucional" class="hidden" autocomplete="off"></label></div>
         <div class="col-md-2 box"><label class="btn btn-primary btn3"><h3>App</h3><img src="img/app.png" alt="..." class="img-thumbnail img-check"><input type="radio" required name="tipo" id="item4" value="app" class="hidden" autocomplete="off"></label></div>
         <div class="col-md-2 box"><label class="btn btn-primary btn5"><h3>Blog</h3><img src="img/blog.png" alt="..." class="img-thumbnail img-check"><input type="radio" required name="tipo" id="item4" value="blog" class="hidden" autocomplete="off"></label></div>
         <div class="col-md-2 box" style="height:200px"></div><br>
         </div>
         <input class="email" placeholder="Email de contacto/usuario" type="email" name="email" value="">
         <input class="password" placeholder="Contraseña" type="password" name="password">
         <input class="password-conf" placeholder="Confirmar Contraseña" type="password" name="password">
         <br>
         <input class="email" placeholder="Pregunta secreta" type="text" name="preg">
         <input class="email" placeholder="Respuesta secreta" type="text" name="resp">
         <div class="clearfix"></div>
         <input class="btn-submit" type="submit" value="Crear Sitio" class="btn btn-success"></center>
       </form>
     <script type="text/javascript">
       $(document).ready(function(e){

         $('.img-check').click(function(e) {
           $('#cant').removeClass('hidden');
           $('.img-check').not(this).removeClass('check')
           .siblings('input').prop('checked',false);
           $(this).addClass('check')
             .siblings('input').prop('checked',true);
           });

         });
         // bind to when the drop-down list changes
     </script>
   </body>
 </html>
