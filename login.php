<!DOCTYPE html>
<?php
session_start();
$datos= file_get_contents("datos.json");
$datos=json_decode($datos,true);
$errores= [];
if ($_POST) {
  if ($_POST["uname"]!==$datos["email"]) {
    $errores[]="Mail no registrado";
  }
  if ($_POST["psw"]!==$datos["password"]) {
    $errores[]="contraseña";
  }
  if (count($errores)==0) {
    $_SESSION["login"] = true;
    header("Location:editor.php");
  }
}
 ?>
<html lang="es" dir="ltr">
<head>
  <meta charset="utf-8">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Source+Sans+Pro:300,400,700" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link rel="stylesheet" href="css/main.css">
  <title>Digital House - Site Builder Log in</title>
  <style media="screen">
  .check
  {
    opacity:0.5;
    color:#996;
  }
  .box{
    margin-bottom:5px;
  }
  </style>
</head>
  <body>
    <center>
    <form action="login.php" method="POST">

  <div class="container">
    <input type="text" class="email" placeholder="Email" name="uname" required>

    <input type="password" class="password" placeholder="Contraseña" name="psw" required>
<br>
    <button type="submit" class="btn-submit">Ingresar</button>
  </div>
</form>
  </body>
</html>
